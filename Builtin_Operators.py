'''
Created on 07.10.2014

@author: renderman
'''
from bpy.props import EnumProperty
from bpy.types import Menu, Operator


#===============================================================================
#    builtin menus
#===============================================================================
class VIEW3D_MT_object_mode(Menu):
	bl_label = "Mode"

	def draw(self, context):
		layout = self.layout

		pie = layout.menu_pie()
		pie.operator_enum("OBJECT_OT_mode_set", "mode")


class VIEW3D_MT_shade(Menu):
	bl_label = "Shade"

	def draw(self, context):
		layout = self.layout

		pie = layout.menu_pie()
		pie.prop(context.space_data, "viewport_shade", expand = True)

		if context.active_object:
			if(context.mode == 'EDIT_MESH'):
				pie.operator("MESH_OT_faces_shade_smooth")
				pie.operator("MESH_OT_faces_shade_flat")
			else:
				pie.operator("OBJECT_OT_shade_smooth")
				pie.operator("OBJECT_OT_shade_flat")


class VIEW3D_OT_manipulator_set(Operator):
	bl_label = "Set Manipulator"
	bl_idname = "view3d.manipulator_set"

	type: EnumProperty(
			name = "Type",
			items = (('TRANSLATE', "Translate", "Use the manipulator for movement transformations"),
					('ROTATE', "Rotate", "Use the manipulator for rotation transformations"),
					('SCALE', "Scale", "Use the manipulator for scale transformations"),
					),
		)

	def execute(self, context):
		#    show manipulator if user selects an option
		context.space_data.show_gizmo_tool = True
		context.space_data.transform_manipulators = {self.type}

		return {'FINISHED'}


class VIEW3D_MT_manipulator(Menu):
	bl_label = "Manipulator"

	def draw(self, context):
		layout = self.layout

		pie = layout.menu_pie()
		pie.operator("view3d.manipulator_set", icon = 'MAN_TRANS', text = "Translate").type = 'TRANSLATE'
		pie.operator("view3d.manipulator_set", icon = 'MAN_ROT', text = "Rotate").type = 'ROTATE'
		pie.operator("view3d.manipulator_set", icon = 'MAN_SCALE', text = "Scale").type = 'SCALE'
		pie.prop(context.space_data, "show_manipulator")


class VIEW3D_MT_pivot(Menu):
	bl_label = "Pivot"

	def draw(self, context):
		layout = self.layout

		pie = layout.menu_pie()
		pie.prop(context.space_data, "pivot_point", expand = True)
		if context.active_object.mode == 'OBJECT':
			pie.prop(context.space_data, "use_pivot_point_align", text = "Center Points")
