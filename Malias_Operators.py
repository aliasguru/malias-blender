'''
Created on 07.10.2014

@author: renderman
'''

import bpy, logging
from bpy.props import EnumProperty, StringProperty
from bpy.types import Operator

logger = logging.getLogger('malias')


#===============================================================================
#    Modelling
#===============================================================================
def reset_gizmos(sd):
	for x in ['show_gizmo_object_translate', 'show_gizmo_object_rotate', 'show_gizmo_object_scale']:
		setattr(sd, x, False)


class VIEW3D_OT_Select_Nothing(Operator):
	bl_label = "Pick Nothing"
	bl_idname = "view3d.select_nothing"

	def execute(self, context:bpy.context):
		reset_gizmos(context.space_data)

		if context.mode == 'EDIT_MESH':
			bpy.ops.mesh.select_all(action = 'DESELECT')
		else:
			bpy.ops.object.select_all(action = 'DESELECT')

		bpy.ops.wm.tool_set_by_id(name = 'builtin.select_box', cycle = False)

		return {'FINISHED'}


class VIEW3D_OT_Select_Component(Operator):
	bl_label = "Pick Component"
	bl_idname = "view3d.select_component"

	type: EnumProperty(
			name = "Type",
			items = (('VERT', "Vertex", "Pick Vertices"),
					('EDGE', "Edge", "Pick Edges"),
					('FACE', "Face", "Pick Faces")),
					)

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):

		try:
			#    store the current mode in the operator first
			if context.mode != 'EDIT_MESH':
				if context.active_object is not None:
					_mode = context.mode

					context.active_object.malias.lastMode = _mode

			#    go to edit mode, so this operator also works on other objects
			#    like e.g. armatures
			bpy.ops.object.mode_set(mode = 'EDIT')

			#    if we handle a mesh, set the selection mode accordingly
			if context.active_object.type == 'MESH':
				reset_gizmos(context.space_data)
				bpy.ops.mesh.select_mode(type = self.type)    #    @UndefinedVariable
		except:
			self.report({'WARNING'}, 'could not switch mode')

		finally:
			bpy.ops.wm.tool_set_by_id(name = 'builtin.select_box', cycle = False)

		return {'FINISHED'}


class WM_OT_Gizmo_Set_By_ID(Operator):
	bl_label = 'Set Gizmo'
	bl_idname = 'wm.gizmo_set_by_id'

	type: EnumProperty(
			name = "Type",
			items = (('MOVE', "Translate", ""),
					('ROTATE', "Rotate", ""),
					('SCALE', "Scale", ""),)
			)

	def execute(self, context:bpy.context):

		sd = context.space_data
		bpy.ops.wm.tool_set_by_id(name = 'builtin.select_box')

		reset_gizmos(sd)

		if self.type == 'SCALE':
			sd.show_gizmo_object_scale = True
		elif self.type == 'ROTATE':
			sd.show_gizmo_object_rotate = True
		else:
			sd.show_gizmo_object_translate = True

		return {'FINISHED'}


class OBJECT_OT_origin_quick_set(Operator):
	bl_label = 'Quick Origin Set'
	bl_idname = 'object.origin_quick_set'

	type: EnumProperty(
			name = "Type",
			items = (('CENTER', "Center", "Center of Scene"),
					('SELECTED', "Selected", "Currently selected item")),
					)

	#    make sure there is a selection
	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		#    save the current cursor position
		_cursor = context.scene.cursor.location.copy()
		_editmode = False
		_mode = context.mode

		#    now set the origin
		if self.type == 'CENTER':
			bpy.ops.view3d.snap_cursor_to_center()

		elif self.type == 'SELECTED':
			bpy.ops.view3d.snap_cursor_to_active()

		#    now set the origin to the new cursor position
		if _mode == 'EDIT_MESH':
			bpy.ops.object.editmode_toggle()
			_editmode = True

		bpy.ops.object.origin_set(type = 'ORIGIN_CURSOR')

		if _editmode:
			bpy.ops.object.editmode_toggle()

		#    restore the original cursor position
		context.scene.cursor.location = _cursor

		return {'FINISHED'}


class OBJECT_OT_display_toggle(Operator):
	bl_label = 'Display Toggle'
	bl_idname = 'object.display_toggle'

	type: EnumProperty(
			name = "Type",
			items = (
					#    ('show_wire', "Wire", "Toggle Wireframe Display"),
					('show_name', "Name", "Toggle Name Display"),
					('show_in_front', "Show In Front", "Toggle Show In Front"),
					('show_bounds', "Bounds", "Toggle Bounding Box Display"),
					#    ('show_all_edges', "Edges", "Toggle All Edges Display"),
					('show_texture_space', "Bounds", "Toggle Texture Space Display"),
					('show_axis', "Axis", "Toggle Axis Display"),
					('draw_type', 'Draw Type', 'Set Maximum Draw Type')
					)
					)

	#    make sure there is a selection
	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		_act = context.active_object
		_val = getattr(_act, self.type)

		#    invert value for selection
		for _obj in context.selected_objects:
			self.SetValue(_obj, not _val)

		#    invert it for active object too (sometimes not included in selection)
		self.SetValue(_act, not _val)

		return {'FINISHED'}

	def SetValue(self, _obj, _value):
		try:
			setattr(_obj, self.type, _value)

			#    for draw wire, draw all edges too
			if self.type == 'show_wire' and _value:
				setattr(_obj, 'show_all_edges', True)

			#    armatures have similar props in their bones, fix namings here
			if _obj.type == 'ARMATURE':
				if self.type == 'show_name':
					#    activate bone names too
					setattr(_obj.data, 'show_names', _value)
				elif self.type == 'show_axis':
					setattr(_obj.data, 'show_axes', _value)

		except:
			self.report({'WARNING'}, 'setting state %s failed for object %s' % (self.type, _obj.name))


class OBJECT_OT_draw_type_set(Operator):
	bl_label = 'Display Toggle'
	bl_idname = 'object.set_draw_type'

	display_type: EnumProperty(
			name = "Type",
			items = (
					('BOUNDS', "Bounds", "Maximum Draw Type: Bounding Box"),
					('WIRE', 'Wire', 'Maximum Draw Type: Wireframe Mesh'),
					('SOLID', 'Solid', 'Maximum Draw Type: Solid Shading'),
					('TEXTURED', 'Textured', 'Maximum Draw Type: Textured Shading')
					))

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		for _obj in context.selected_editable_objects:
			_obj.display_type = self.display_type
		return {'FINISHED'}

#===============================================================================
#    Painting
#===============================================================================


class BRUSH_OT_Switch_Brush(Operator):
	bl_label = 'Switch Brush'
	bl_idname = 'brush.switch_brush'

	type: StringProperty(name = 'Type',
						description = 'Brush Type',
						default = 'Draw')

	def execute(self, context):
		if self.type in bpy.data.brushes:
			context.tool_settings.vertex_paint.brush = bpy.data.brushes[self.type]
			context.tool_settings.weight_paint.brush = bpy.data.brushes[self.type]
			context.tool_settings.image_paint.brush = bpy.data.brushes[self.type]
			context.tool_settings.sculpt.brush = bpy.data.brushes[self.type]
		else:
			self.report({'ERROR'}, '%s Brush does not exist, please create it' % self.type)
			return {'CANCELLED'}

		return {'FINISHED'}


class BRUSH_OT_Stroke_Method(Operator):
	bl_label = 'Stroke Method'
	bl_idname = 'brush.stroke_method'

	type: EnumProperty(name = 'Type',
					items = (
						('Space', 'SPACE', 'Space Method'),
						('Dots', 'DOTS', 'Dots Method'),
						('Airbrush', 'AIRBRUSH', 'Airbrush Method')
						))

	def execute(self, context):
		context.tool_settings.vertex_paint.brush.stroke_method = self.type.upper()
		context.tool_settings.weight_paint.brush.stroke_method = self.type.upper()
		context.tool_settings.image_paint.brush.stroke_method = self.type.upper()
		context.tool_settings.sculpt.brush.stroke_method = self.type.upper()
		return {'FINISHED'}


#===============================================================================
#    UV Editing / Image Editor
#===============================================================================
class UV_OT_Select_Component(Operator):
	bl_label = "Pick Component"
	bl_idname = "uv.select_component"

	type: EnumProperty(
			name = "Type",
			items = (('VERTEX', "Vertex", "Pick Vertices"),
					('EDGE', "Edge", "Pick Edges"),
					('FACE', "Face", "Pick Faces"),
					('ISLAND', 'Island', 'Pick UV Island'),
					('TOGGLESYNC', 'ToggleSync', 'Toggle UV Synchronisation')
					))

	def execute(self, context):

		try:
			bpy.ops.object.mode_set(mode = 'EDIT')

			if self.type != 'TOGGLESYNC':
				bpy.data.scenes[context.scene.name].tool_settings.uv_select_mode = self.type
			else:
				_sync = bpy.data.scenes[context.scene.name].tool_settings
				if _sync.use_uv_select_sync:
					bpy.ops.mesh.select_all(action = 'SELECT')
				_sync.use_uv_select_sync = not _sync.use_uv_select_sync

		except Exception as _exc:
			self.report({'WARNING'}, 'could not switch mode: %s' % _exc)

		return {'FINISHED'}


class UV_OT_Tool_Settings(Operator):
	bl_label = "Change Tool Settings"
	bl_idname = "uv.tool_settings"

	type: EnumProperty(
			name = "Type",
			items = (('UV_SCULPT', "UV Sculpt", "Toggle UV Sculpt"),
					('PROPORTIONAL', 'Proportional Toggle', 'Toggle Proportional Editing'),

					))

	def execute(self, context):

		bpy.ops.object.mode_set(mode = 'EDIT')
		if self.type == 'UV_SCULPT':
			context.tool_settings.use_uv_sculpt = not context.tool_settings.use_uv_sculpt
		elif self.type == 'PROPORTIONAL':
			if context.tool_settings.proportional_edit != 'CONNECTED':
				context.tool_settings.proportional_edit = 'CONNECTED'
				context.tool_settings.proportional_edit_falloff = 'LINEAR'
			else:
				context.tool_settings.proportional_edit = 'DISABLED'

		return {'FINISHED'}


class UV_OT_Editor_Settings(Operator):
	bl_label = "Change Editor Settings"
	bl_idname = "uv.editor_settings"

	scope: EnumProperty(
			name = "Scope",
			items = (('STICKY_SELECT_MODE', "Sticky Select Mode", "Toggle Sticky Select Mode"),
					('STRETCH', 'Stretch Visualisation', 'Toggle Stretch Visualisation'),
					('EDGE_DRAW_TYPE', 'Edge Draw Type', 'Cycle Edge Draw Type')
					))

	types = ['OUTLINE', 'DASH', 'BLACK', 'WHITE']

	def execute(self, context):

		_uved = context.space_data.uv_editor

		if self.scope == 'STICKY_SELECT_MODE':
			if _uved.sticky_select_mode != 'DISABLED':
				_uved.sticky_select_mode = 'DISABLED'
				self.report({'INFO'}, 'individual UV selection enabled')
			else:
				_uved.sticky_select_mode = 'SHARED_LOCATION'
				self.report({'INFO'}, 'global UV selection enabled')
		elif self.scope == 'STRETCH':
			_uved.show_stretch = not _uved.show_stretch
			_uved.display_stretch_type = 'AREA'
		else:
			logger.debug(_uved.edge_display_type)
			_index = self.types.index(_uved.edge_display_type)

			_index += 1
			if _index > 3: _index = 0

			_uved.edge_display_type = self.types[_index]

		return {'FINISHED'}

