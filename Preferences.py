'''
Created on 31.10.2014
@author: r.trummer
'''

import bpy, logging, os

import addon_utils    #    @UnresolvedImport
from bpy.props import BoolProperty
from bpy.types import AddonPreferences

logger = logging.getLogger('malias')


#===============================================================================
#    global Add-on preference code
#===============================================================================
class Malias_Preferences(AddonPreferences):
	bl_idname = __package__

	use_gizmos: BoolProperty(default = True,
							name = 'Use Gizmos Instead Of TRS Tools',
							description = 'Instead of switching the Translate, Rotate or Scale Tool, enable corresponding Gizmos for Select Tool')

	def draw(self, context):
		layout = self.layout
		layout.prop(self, 'use_gizmos')


#===============================================================================
#    FUNCTIONS for retrieving Prefs
#===============================================================================
def get_module_name():
	'''
	return name of this module
	'''
	return __name__.split(sep = '.')[0]


def DefineKeymap(addon_keymaps):
	'''
	adds custom keys to Blender which make the user experience similar to Maya
	:param addon_keymaps: List for tracking added keymaps, needed for unregistering them
	'''

	#    assign key configurations to them
	wm = bpy.context.window_manager    #    @UndefinedVariable
	kc = wm.keyconfigs.addon

	logger.info('============ DEFINING MALIAS KEYMAPS ===========')

	if kc:
		#    define strings for AutoCompletion
		__press__ = 'PRESS'
		__click_drag__ = 'CLICK_DRAG'
		__left_mouse__ = 'LEFTMOUSE'
		__middle_mouse__ = 'MIDDLEMOUSE'
		__right_mouse__ = 'RIGHTMOUSE'
		__select_mouse__ = 'LEFTMOUSE'
		__tweak__ = 'TWEAK'

		km_onm = kc.keymaps.new(name = 'Object Non-modal')
		km_3dg = kc.keymaps.new(name = '3D View', space_type = 'VIEW_3D', region_type = 'WINDOW', modal = False)
		km_mesh = kc.keymaps.new(name = 'Mesh')
		km_weight_paint = kc.keymaps.new(name = 'Weight Paint')
		km_vertex_paint = kc.keymaps.new(name = 'Vertex Paint')
		km_image_paint = kc.keymaps.new(name = 'Image Paint')
		km_sculpt = kc.keymaps.new(name = 'Sculpt')
		km_uv = kc.keymaps.new(name = 'UV Editor')
		km_window = kc.keymaps.new(name = 'Window')
		km_2dview = kc.keymaps.new(name = 'View2D')
		km_2dview_buttons = kc.keymaps.new(name = 'View2D Buttons List')
		km_image = kc.keymaps.new(name = 'Image', space_type = 'IMAGE_EDITOR')
		km_node_editor = kc.keymaps.new(name = 'Node Editor', space_type = 'NODE_EDITOR')
		km_screen = kc.keymaps.new(name = 'Screen')

		wm_pie = 'wm.call_menu_pie'

		#===============================================================================
		#    Global hotkeys
		#===============================================================================
		kmi = km_screen.keymap_items.new(idname = 'ed.undo_history', type = 'Z', value = __press__, ctrl = True, alt = True)

		#=======================================================================
		#    3D View Pie Menus
		#=======================================================================
		kmi = km_3dg.keymap_items.new(idname = wm_pie, type = __left_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_lmb_object'
		kmi = km_3dg.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_mmb_object'
		kmi = km_3dg.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_rmb_object'

		#===============================================================
		#    3D View Hotkeys
		#===============================================================
		kmi = km_3dg.keymap_items.new('conversion_cleanup.select_and_isolate_pattern', type = 'X', value = __press__, ctrl = True, shift = True)
		kmi = km_3dg.keymap_items.new('conversion_cleanup.visibility_material_add', type = 'V', value = __press__, ctrl = True, shift = True)

		#    add view switching tweaks
		#    alt middle is taken by Malias, so use Shift + Alt + Left instead
		__viewDict__ = {'NORTH': 'TOP',
					'SOUTH':'BOTTOM',
					'EAST':'RIGHT',
					'WEST':'LEFT'}

		for _v in __viewDict__:
			if bpy.app.version > (3, 1, 2):
				kmi = km_3dg.keymap_items.new('view3d.view_axis', type = __left_mouse__, value = __click_drag__, any = False, shift = True, alt = True)
				kmi.direction = _v
			else:
				kmi = km_3dg.keymap_items.new('view3d.view_axis', type = __left_mouse__, value = _v, any = False, shift = True, alt = True)
				kmi.map_type = __tweak__
				kmi.value = _v
			kmi.properties.relative = True
			kmi.properties.type = __viewDict__[_v]

		#===============================================================
		#    Maya interaction Style Hotkeys
		#===============================================================
		kmi = km_3dg.keymap_items.new('view3d.rotate', type = __left_mouse__, value = __press__, alt = True)
		kmi = km_3dg.keymap_items.new('view3d.move', type = __middle_mouse__, value = __press__, alt = True)
		kmi = km_3dg.keymap_items.new('view3d.zoom', type = __right_mouse__, value = __press__, alt = True)

		#===============================================================
		#    Mesh Edit Mode Hotkeys
		#===============================================================
		kmi = km_mesh.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_mmb_mesh'
		kmi = km_mesh.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_rmb_mesh'
		kmi = km_mesh.keymap_items.new(idname = 'transform.vert_crease', type = 'E', value = __press__, shift = True, alt = True)
		kmi.properties.value = 1

		#=======================================================================
		#    Weight paint hotkeys
		#=======================================================================
		kmi = km_weight_paint.keymap_items.new(idname = wm_pie, type = __left_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_lmb_vertex_paint'
		kmi = km_weight_paint.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_mmb_vertex_paint'
		kmi = km_weight_paint.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_rmb_weight'
		kmi = km_weight_paint.keymap_items.new('view3d.rotate', type = __left_mouse__, value = __press__, alt = True)
		kmi = km_weight_paint.keymap_items.new('paint.weight_gradient', type = __left_mouse__, value = __press__, alt = True, shift = True)

		#=======================================================================
		#    Vertex paint hotkeys
		#=======================================================================
		kmi = km_vertex_paint.keymap_items.new(idname = wm_pie, type = __left_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_lmb_vertex_paint'
		kmi = km_vertex_paint.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_mmb_vertex_paint'
		kmi = km_vertex_paint.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_rmb_weight'

		#=======================================================================
		#    Sculpting hotkeys
		#=======================================================================
		kmi = km_sculpt.keymap_items.new(idname = wm_pie, type = __left_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_lmb_vertex_paint'
		kmi = km_sculpt.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_mmb_sculpt'
		kmi = km_sculpt.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_rmb_sculpt'

		kmi = km_sculpt.keymap_items.new('brush.stencil_control', type = __left_mouse__, value = __press__, shift = True, alt = True)
		kmi.properties.mode = 'ROTATION'
		kmi = km_sculpt.keymap_items.new('brush.stencil_control', type = __middle_mouse__, value = __press__, shift = True, alt = True)
		kmi.properties.mode = 'TRANSLATION'
		kmi = km_sculpt.keymap_items.new('brush.stencil_control', type = __right_mouse__, value = __press__, shift = True, alt = True)
		kmi.properties.mode = 'SCALE'
		kmi = km_sculpt.keymap_items.new('view3d.zoom', type = __right_mouse__, value = __press__, alt = True)

		#===============================================================
		#    UV Mapping Hotkeys
		#===============================================================
		kmi = km_uv.keymap_items.new(idname = wm_pie, type = __left_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'UV_MT_malias_lmb_uv'
		kmi = km_uv.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'UV_MT_malias_mmb_uv'
		kmi = km_uv.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'UV_MT_malias_rmb_uv'

		#=======================================================================
		#    Window hotkeys
		#=======================================================================
		kmi = km_window.keymap_items.new('wm.console_toggle', type = 'F11', value = __press__, shift = True, alt = True, ctrl = True)
		kmi = km_window.keymap_items.new('import_scene.multiple_fbx', type = 'I', value = __press__, shift = True, ctrl = True)
		kmi = km_window.keymap_items.new('import_scene.fbx', type = 'I', value = __press__, shift = True, alt = True)
		kmi = km_window.keymap_items.new('export_scene.fbx', type = 'E', value = __press__, shift = True, ctrl = True)
		kmi = km_window.keymap_items.new('script.reload', type = 'F8', value = __press__)

		#=======================================================================
		#    2D Window Hotkeys
		#=======================================================================
		kmi = km_2dview.keymap_items.new('view2d.pan', type = __left_mouse__, value = __press__, alt = True)
		kmi = km_2dview.keymap_items.new('view2d.pan', type = __middle_mouse__, value = __press__, alt = True)
		kmi = km_2dview.keymap_items.new('view2d.zoom', type = __right_mouse__, value = __press__, alt = True)

		#=======================================================================
		#    2D Buttons List Hotkeys
		#=======================================================================
		kmi = km_2dview_buttons.keymap_items.new('view2d.pan', type = __left_mouse__, value = __press__, alt = True)
		kmi = km_2dview_buttons.keymap_items.new('view2d.pan', type = __middle_mouse__, value = __press__, alt = True)
		kmi = km_2dview_buttons.keymap_items.new('view2d.zoom', type = __right_mouse__, value = __press__, alt = True)

		#=======================================================================
		#    Image Editor Hotkeys
		#=======================================================================
		kmi = km_image.keymap_items.new('image.view_pan', type = __left_mouse__, value = __press__, alt = True)
		kmi = km_image.keymap_items.new('image.view_pan', type = __middle_mouse__, value = __press__, alt = True)
		kmi = km_image.keymap_items.new('image.view_zoom', type = __right_mouse__, value = __press__, alt = True)

		#=======================================================================
		#    Hotkeys for Addons
		#=======================================================================
		kmi = km_mesh.keymap_items.new('mesh.straighten_vertex_path', type = 'ONE', value = __press__, shift = True, alt = True)
		kmi = km_mesh.keymap_items.new('mesh.looptools_curve', type = 'C', value = __press__, shift = True, ctrl = True)
		kmi = km_mesh.keymap_items.new('mesh.looptools_flatten', type = 'F', value = __press__, shift = True, ctrl = True)
		kmi = km_mesh.keymap_items.new('mesh.looptools_relax', type = 'R', value = __press__, shift = True, ctrl = True)
		kmi = km_mesh.keymap_items.new('mesh.looptools_space', type = 'R', value = __press__, shift = True, ctrl = True, alt = True)

		kmi = km_node_editor.keymap_items.new('node.nw_lazy_mix', type = __right_mouse__, value = __press__, shift = True, ctrl = True, alt = True)
		kmi = km_node_editor.keymap_items.new('node.backimage_move', type = __left_mouse__, value = __press__, shift = True, alt = True)
		kmi = km_node_editor.keymap_items.new('node.backimage_move', type = __middle_mouse__, value = __press__, shift = True, alt = True)
		kmi = km_node_editor.keymap_items.new('node.backimage_zoom', type = 'NUMPAD_MINUS', value = __press__, shift = True, alt = True)
		kmi.properties.factor = 0.8
		kmi = km_node_editor.keymap_items.new('node.backimage_zoom', type = 'NUMPAD_PLUS', value = __press__, shift = True, alt = True)
		kmi.properties.factor = 1.2
		kmi = km_node_editor.keymap_items.new('node.nw_lazy_connect', type = __right_mouse__, value = __press__, shift = True, alt = True)
		kmi = km_node_editor.keymap_items.new('node.nw_bg_reset', type = 'HOME', value = __press__, shift = True, alt = True)
		kmi = km_node_editor.keymap_items.new('node.add_reroute', type = __left_mouse__, value = __press__, shift = True)
		if bpy.app.version < (3, 2, 0): kmi.map_type = __tweak__
		kmi = km_node_editor.keymap_items.new('node.links_cut', type = __left_mouse__, value = __press__, ctrl = True)
		if bpy.app.version < (3, 2, 0): kmi.map_type = __tweak__

		#=======================================================================
		#    Image Paint Hotkeys
		#=======================================================================
		kmi = km_image_paint.keymap_items.new(idname = wm_pie, type = __middle_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_mmb_image_paint'
		kmi = km_image_paint.keymap_items.new(idname = wm_pie, type = __right_mouse__, value = __press__, ctrl = True, shift = True)
		kmi.properties.name = 'VIEW3D_MT_malias_rmb_image_paint'

		kmi = km_image_paint.keymap_items.new('brush.stencil_control', type = __left_mouse__, value = __press__, shift = True, alt = True)
		kmi.properties.mode = 'ROTATION'
		kmi = km_image_paint.keymap_items.new('brush.stencil_control', type = __middle_mouse__, value = __press__, shift = True, alt = True)
		kmi.properties.mode = 'TRANSLATION'
		kmi = km_image_paint.keymap_items.new('brush.stencil_control', type = __right_mouse__, value = __press__, shift = True, alt = True)
		kmi.properties.mode = 'SCALE'
		kmi = km_image_paint.keymap_items.new('view3d.zoom', type = __right_mouse__, value = __press__, alt = True)

		#    for debugging reasons, print the last created keymap properties
		logger.debug('Last Keymap Item Properties: %s' % dir(kmi.properties))

		#=======================================================================
		#    Append the Keymaps to the System
		#=======================================================================
		addon_keymaps.append(km_onm)
		addon_keymaps.append(km_3dg)
		addon_keymaps.append(km_mesh)
		addon_keymaps.append(km_weight_paint)
		addon_keymaps.append(km_vertex_paint)
		addon_keymaps.append(km_uv)
		addon_keymaps.append(km_window)
		addon_keymaps.append(km_2dview)
		addon_keymaps.append(km_2dview_buttons)
		addon_keymaps.append(km_image)
		addon_keymaps.append(km_node_editor)
		addon_keymaps.append(km_image_paint)

		logger.info('============ MALIAS KEYMAPS DEFINED ===========')

	return addon_keymaps


def DefinePreferences():
	'''
	Creates Main User Preferences for consistency
	'''
	logger.info('============ DEFINING MALIAS PREFERENCES===========')

	_mainPrefs = bpy.context.preferences
	_mainPrefs.inputs.use_mouse_continuous = True
	_mainPrefs.inputs.view_zoom_axis = 'HORIZONTAL'
	_mainPrefs.inputs.drag_threshold = 20
	_mainPrefs.inputs.use_auto_perspective = True
	_mainPrefs.inputs.use_drag_immediately = True

	_mainPrefs.edit.material_link = 'OBJECT'
	_mainPrefs.edit.use_global_undo = True
	_mainPrefs.edit.undo_memory_limit = 2048
	_mainPrefs.edit.undo_steps = 64

	_mainPrefs.view.smooth_view = 200
	_mainPrefs.view.show_splash = True
	_mainPrefs.view.use_save_prompt = True
	_mainPrefs.view.pie_menu_radius = 120
	_mainPrefs.view.pie_menu_threshold = 36
	_mainPrefs.view.pie_animation_timeout = 7

	_mainPrefs.system.use_region_overlap = True

	_mainPrefs.filepaths.save_version = 5
	_mainPrefs.filepaths.recent_files = 20
	_mainPrefs.filepaths.render_cache_directory = '//RenderCache/'
	_mainPrefs.filepaths.use_file_compression = True
	_mainPrefs.filepaths.use_load_ui = True
	_mainPrefs.filepaths.render_output_directory = '//Renders/TestRender'
	_mainPrefs.filepaths.use_scripts_auto_execute = True
	_mainPrefs.filepaths.use_tabs_as_spaces = False

	_mainPrefs.system.use_select_pick_depth = True

	#===========================================================================
	#    user specific preferences come here
	#===========================================================================
	_exceptionUsers = ['j.harris']
	_developers = []
	_knowswhatheisdoing = ['j.berger', 'r.trummer']

	try:
		_currentUser = os.getlogin()

		logger.debug('setting user specific prefs, user is %s' % _currentUser)

		#    first, get the exception users
		if _currentUser in _exceptionUsers:
			_mainPrefs.view.use_zoom_to_mouse = True

		if _currentUser in _developers:
			bpy.app.debug = True

		if _currentUser in _knowswhatheisdoing:
			pass
			#    _mainPrefs.filepaths.use_load_ui = True

	except:
		pass

	logger.info('============ MALIAS PREFERENCES DEFINED ===========')


def EnableAddons():
	'''
	enables certain addons which are beneficial to malias
	be aware that this can crash with some of the addons if they
	are not found or out of date
	'''

	_addonList = [
		'node_wrangler',
		'io_conversion_import_tools',
		'io_creo_translate',
		'io_import_multiple_collada',
		'mesh_apply_modifiers',
		'mesh_easy_lattice',
		'mesh_straighten_vertex_path',
				]

	logger.info('============ MALIAS ADDON REGISTRATION ===========')

	for _addon in _addonList:
		if not addon_utils.check(_addon)[1] and not addon_utils.check(_addon)[0]:
			try:
				logger.debug('re-enabling addon %s' % _addon)
				addon_utils.enable(_addon, persistent = True, default_set = True)

			except Exception as _exc:
				logger.warning('Unable to activate malias preferred addon %s, cause: %s' % (_addon, _exc))
