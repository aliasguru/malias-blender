'''
Created on 15.09.2015

@author: renderman
'''

from bpy.props import StringProperty
from bpy.types import PropertyGroup


#===============================================================================
#    store scene properties here
#===============================================================================
class MaliasObjectProperties(PropertyGroup):

	def GetLastMode(self):
		#    unfortunately, there is inconsistency within Blender regarding mode naming
		#    to fix this, we try to shuffle names around. PAINT_VERTEX becomes VERTEX_PAINT etc.
		#    even more stupid, MESH_EDIT becomes EDIT, PARTICLE becomes PARTICLE_EDIT, etc.
		#    another problem: modes can disappear! POSE and PARTICLE_EDIT are two examples
		#    to fix this, the last mode is stored within the object, not within the scene
		if 'LastMode' in self:

			_mode = self['LastMode']
			_mode = _mode if '_' not in _mode else '%s_%s' % (_mode.split('_')[1], _mode.split('_')[0])
			return _mode
		else:
			return 'OBJECT'

	def SetLastMode(self, value):
		self['LastMode'] = value

	lastMode: StringProperty(
							name = 'Last Mode',
							description = 'stores the last mode that the model was in before the user jumped into edit mode',
							default = 'OBJECT',
							get = GetLastMode,
							set = SetLastMode
							)
