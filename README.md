# README #

Malias for Blender is an attempt to make the switch to Blender easier for existing users of Alias and/or Maya. It implements the well known Marking Menus of Alias using `Ctrl` + `Shift` + `Mousebutton L / M / R` to spawn Blenders Pie Menus.

### What is this repository for? ###

* switch Blender to `LMB` selection
* swap many of the Blender tools from `LMB` to `RMB`
* add Pie Menus in 3D View, UV Image Editor and (later target) Node Editor  

### How do I get set up? ###

* Download the Zip [here](https://bitbucket.org/aliasguru/malias-blender/get/6e7b0933d8b5.zip)
* run Blender
* Goto `File -> User Preferences -> Add-ons`
* Click `Install from File`
* select the downloaded Zip file
* confirm the dialogue
* check `Malias for Blender`
* Click `Save User Settings`

### Contribution guidelines ###

* no contributions planned yet

### Who do I talk to? ###

* Rainer Trummer aka aliasguru
* leave a message in the Issue Tracker in case of bugs