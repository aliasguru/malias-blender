'''
Created on 07.10.2014

@author: renderman
'''
# ##### BEGIN GPL LICENSE BLOCK #####
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software Foundation,
#    Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#    <pep8 compliant>
import logging
import sys, os, faulthandler

faulthandler.enable()
logger = logging.getLogger('malias')

#===============================================================================
#    reload module just in case
#===============================================================================
if 'bpy' in locals():
	import importlib
	for x in [Properties, Preferences, Builtin_Operators, Malias_Operators, pie_menus_Rainer]:    #    @UndefinedVariable
		importlib.reload(x)

else:
	import bpy
	from . import Properties, Builtin_Operators, Malias_Operators, pie_menus_Rainer, Preferences

#===============================================================================
#    import main _malias classes
#===============================================================================

bl_info = {
	"name": "Pie Menus Malias",
	"author": "Rainer Trummer",
	"version": (1, 4, 0),
	"blender": (3, 3, 0),
	"description": "Enable _malias Pie Menus and Palette in blender",
	"category": "User Interface",
	"wiki_url": "https://bitbucket.org/aliasguru/malias-blender/overview",
	"tracker_url":"https://bitbucket.org/aliasguru/malias-blender/issues?status=new&status=open"
}

addon_keymaps = []

classes = (
	Builtin_Operators.VIEW3D_MT_object_mode,
	Builtin_Operators.VIEW3D_MT_shade,
	Builtin_Operators.VIEW3D_OT_manipulator_set,
	Builtin_Operators.VIEW3D_MT_manipulator,
	Builtin_Operators.VIEW3D_MT_pivot,

	Malias_Operators.VIEW3D_OT_Select_Nothing,
	Malias_Operators.VIEW3D_OT_Select_Component,
	Malias_Operators.OBJECT_OT_origin_quick_set,
	Malias_Operators.OBJECT_OT_display_toggle,
	Malias_Operators.OBJECT_OT_draw_type_set,
	Malias_Operators.BRUSH_OT_Switch_Brush,
	Malias_Operators.BRUSH_OT_Stroke_Method,
	Malias_Operators.UV_OT_Select_Component,
	Malias_Operators.UV_OT_Tool_Settings,
	Malias_Operators.UV_OT_Editor_Settings,
	Malias_Operators.WM_OT_Gizmo_Set_By_ID,

	pie_menus_Rainer.VIEW3D_MT_malias_lmb_object,
	pie_menus_Rainer.VIEW3D_MT_malias_lmb_vertex_paint,
	pie_menus_Rainer.VIEW3D_MT_malias_rmb_object,
	pie_menus_Rainer.VIEW3D_MT_malias_mmb_object,
	pie_menus_Rainer.VIEW3D_MT_malias_mmb_mesh,
	pie_menus_Rainer.VIEW3D_MT_malias_mmb_sculpt,
	pie_menus_Rainer.VIEW3D_MT_malias_mmb_image_paint,
	pie_menus_Rainer.VIEW3D_MT_malias_mmb_vertex_paint,
	pie_menus_Rainer.VIEW3D_MT_malias_mmb_weight_paint,
	pie_menus_Rainer.VIEW3D_MT_malias_rmb_mesh,
	pie_menus_Rainer.VIEW3D_MT_malias_rmb_sculpt,
	pie_menus_Rainer.VIEW3D_MT_malias_rmb_weight,
	pie_menus_Rainer.VIEW3D_MT_malias_rmb_image_paint,
	pie_menus_Rainer.UV_MT_malias_lmb_uv,
	pie_menus_Rainer.UV_MT_malias_mmb_uv,
	pie_menus_Rainer.UV_MT_malias_rmb_uv,
	pie_menus_Rainer.VIEW3D_MT_malias_generative_modifiers,

	Properties.MaliasObjectProperties,
	Preferences.Malias_Preferences,
	)


def bg_check():

	if bpy.app.background:
		logger.warning(f'running in background mode, skipping registering {__package__}')

	return bpy.app.background


def register():
	if bg_check(): return

	for cl in classes:
		if not hasattr(bpy.types, cl.__name__):
			logger.debug('registering class %s' % cl.__name__)
			bpy.utils.register_class(cl)
		else:
			logger.error(f'cannot register class {cl.__name__}, name is taken')

	#    reference the Malias Properties to all the Objects in a scene
	bpy.types.Object.malias = bpy.props.PointerProperty(type = Properties.MaliasObjectProperties)

	#    define preferences
	Preferences.EnableAddons()
	Preferences.DefinePreferences()

	#    assign key configurations to them
	Preferences.DefineKeymap(addon_keymaps)


def unregister():
	if bg_check(): return

	#===========================================================================
	#    remove all of the key configurations in a loop
	#===========================================================================
	wm = bpy.context.window_manager    #    @UndefinedVariable

	if wm.keyconfigs.addon:
		for km in addon_keymaps:
			for kmi in km.keymap_items:
				logger.debug (f'unregistering keymap {kmi}')
				km.keymap_items.remove(kmi)

	#    clear the list
	addon_keymaps.clear()

	#    remove the custom properties as well
	del bpy.types.Object.malias

	for cl in reversed(classes):
		logger.debug(f'unregistering class {cl.__name__}')
		bpy.utils.unregister_class(cl)


if __name__ == '__main__':
	register()
