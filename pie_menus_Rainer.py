# ##### BEGIN GPL LICENSE BLOCK #####
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software Foundation,
#    Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

#    <pep8 compliant>

import bpy
from bpy.types import Menu
from . import Malias_Operators as MO


#===============================================================================
#    Rainer Left Mouse Button Menu
#    serves as a base class for most pie menus in Malias for Blender
#===============================================================================
class MaliasPie_Base():

	def __init__(self):
		self.pie = self.layout.menu_pie()

	def draw(self, context):

		#    W
		self.West(context)

		#    E
		self.East(context)

		#    S
		self.South(context)

		#    N
		self.North(context)

		#    NW
		self.NorthWest(context)

		#    NE
		self.NorthEast(context)

		#    SW
		self.SouthWest(context)

		#    SE
		self.SouthEast(context)

	def West(self, context):
		self.pie.separator()

	def East(self, context):
		self.pie.separator()

	def North(self, context):
		self.pie.separator()

	def South(self, context):
		self.pie.separator()

	def NorthWest(self, context):
		self.pie.separator()

	def NorthEast(self, context):
		self.pie.separator()

	def SouthWest(self, context):
		self.pie.separator()

	def SouthEast(self, context):
		self.pie.separator()


class SetToolExtension():

	def SetToolByName(self, *args, **kwargs):
		toolname:str = kwargs.setdefault('name', 'Box Select')
		prefs = bpy.context.preferences.addons[__package__].preferences

		#    due to API changes, the name is not the name any longer. an ID has to be passed on instead
		#    format at the moment is 'builtin,name_of_tool
		#    make a nice name out of that, since at the moment I cannot query a tool
		_toolname = toolname.split('.')[-1].replace('_', ' ').title()
		tooltitle = kwargs.setdefault('title', _toolname)
		_icon = kwargs.setdefault('icon', 'NONE')

		if prefs.use_gizmos and _toolname in ('Move', 'Rotate', 'Scale'):
			x = self.pie.operator('wm.gizmo_set_by_id', text = tooltitle, icon = _icon)
			x.type = _toolname.upper()
		else:
			x = self.pie.operator('wm.tool_set_by_id', text = tooltitle, icon = _icon)
			x.name = toolname

		return lambda y: x


#===============================================================================
#    3D view Pie Menus
#    defining one per type now
#    more code, but cleaner to maintain
#===============================================================================
class VIEW3D_MT_malias_lmb_base(MaliasPie_Base, SetToolExtension):

	def West(self, context):
		self.pie.operator('view3d.select_component', text = 'Select Edges', icon = 'EDGESEL').type = 'EDGE'

	def East(self, context):
		self.pie.operator('view3d.select_component', text = 'Select Vertices', icon = 'VERTEXSEL').type = 'VERT'

	def South(self, context):
		_mode = context.active_object.malias.lastMode if hasattr(context.object, 'malias') else 'OBJECT'
		self.pie.operator('object.mode_set', text = 'Select Object', icon = 'MESH_DATA').mode = _mode

	def North(self, context):
		self.pie.operator(MO.VIEW3D_OT_Select_Nothing.bl_idname, text = 'Select Nothing', icon = 'RESTRICT_SELECT_ON')

	def NorthWest(self, context):
		self.SetToolByName(name = 'builtin.select_circle', icon = 'MESH_CIRCLE')

	def NorthEast(self, context):
		self.SetToolByName(name = 'builtin.select_box', icon = 'BORDERMOVE')

	def SouthWest(self, context):
		self.pie.operator('view3d.localview', text = 'Local View', icon = 'VIEW3D')

	def SouthEast(self, context):
		self.pie.operator(MO.VIEW3D_OT_Select_Component.bl_idname, text = 'Select Faces', icon = 'FACESEL').type = 'FACE'


#===============================================================================
#    Object Mode
#===============================================================================
class VIEW3D_MT_malias_lmb_object(VIEW3D_MT_malias_lmb_base, Menu):
	bl_label = "Select"


#===============================================================================
#    Vertex Paint LMB
#===============================================================================
class VIEW3D_MT_malias_lmb_vertex_paint(VIEW3D_MT_malias_lmb_base, Menu):
	bl_label = "Select"

	def North(self, context):
		self.pie.operator(MO.BRUSH_OT_Stroke_Method.bl_idname, text = 'Stroke Method: Space', icon = 'MOD_VERTEX_WEIGHT').type = 'Space'

	def NorthWest(self, context):
		self.pie.operator(MO.BRUSH_OT_Stroke_Method.bl_idname, text = 'Stroke Method: Dots', icon = 'GROUP_VERTEX').type = 'Dots'

	def NorthEast(self, context):
		self.pie.operator(MO.BRUSH_OT_Stroke_Method.bl_idname, text = 'Stroke Method: Airbrush', icon = 'BRUSH_DATA').type = 'Airbrush'


#===============================================================================
#    MMB Pie Menus
#===============================================================================
class VIEW3D_MT_malias_mmb_base(MaliasPie_Base, SetToolExtension):

	def West(self, context):
		self.pie.operator('view3d.snap_cursor_to_selected', text = 'Cursor to Selected', icon = 'PIVOT_CURSOR')

	def East(self, context):
		self.pie.operator('wm.call_menu_pie', text = 'Add Generative Modifier', icon = 'TRIA_RIGHT').name = 'VIEW3D_MT_malias_generative_modifiers'

	def South(self, context):
		col = self.pie.column(align = True)
		col.operator('object.origin_set', text = 'Set Origin to Cursor', icon = 'PIVOT_CURSOR').type = 'ORIGIN_CURSOR'
		col.operator('object.origin_set', text = 'Set Origin to Center of Mass', icon = 'FORCE_FORCE').type = 'ORIGIN_CENTER_OF_MASS'
		col.operator('object.origin_quick_set', text = 'Pivot to Active', icon = 'PIVOT_CURSOR').type = 'SELECTED'

		col.separator()

		col.prop(context.preferences.addons[__package__].preferences, 'use_gizmos', icon = 'OBJECT_ORIGIN')

	def North(self, context):
		self.SetToolByName(name = 'builtin.move')

	def NorthWest(self, context):
		self.pie.operator('view3d.snap_selected_to_cursor', text = 'Selection to Cursor', icon = 'PIVOT_CURSOR')

	def NorthEast(self, context):
		self.SetToolByName(name = 'builtin.rotate')

	def SouthWest(self, context):
		self.pie.operator('object.origin_quick_set', text = 'Nullify Pivot', icon = 'PIVOT_CURSOR').type = 'CENTER'

	def SouthEast(self, context):
		self.SetToolByName(name = 'builtin.scale')


#===============================================================================
#    Object Mode MMB
#===============================================================================
class VIEW3D_MT_malias_mmb_object(VIEW3D_MT_malias_mmb_base, Menu):
	bl_label = "Transform"


#===============================================================================
#    Mesh Edit Mode MMB
#===============================================================================
class VIEW3D_MT_malias_mmb_mesh(VIEW3D_MT_malias_mmb_base, Menu):
	bl_label = "Transform"

	def East(self, context):
		self.SetToolByName(name = 'builtin.shrink_fatten', icon = 'FULLSCREEN_ENTER')


#===============================================================================
#    Sculpt, Vertex, Texture and Weight Paint Mode MMB Base
#===============================================================================
class VIEW3D_MT_malias_mmb_sculpt_base(VIEW3D_MT_malias_mmb_base):

	CheckBrushes = []

	def draw(self, context):
		#    find all sculpting brushes
		_brushes = self.Brushes.copy()
		_brushes.insert(2, [x.name for x in self.CheckBrushes if x.name not in _brushes])

		for _brush in _brushes:
			self.brushAdder(_brush, self.Icon)

	#    add function to generate a brush pie menu item
	def brushAdder(self, _brush, _icon = 'BRUSH_DATA'):

		if type(_brush) == list:
			col = self.pie.column(align = True)
			for _b in _brush:
				col.operator(MO.BRUSH_OT_Switch_Brush.bl_idname,
							text = '%s Brush' % (_b if _b != 'Brush' else 'Default'),
							icon = _icon
							).type = _b

		else:
			self.pie.operator(MO.BRUSH_OT_Switch_Brush.bl_idname,
							text = '%s Brush' % (_brush if _brush != 'Brush' else 'Default'),
							icon = _icon
							).type = _brush


#===============================================================================
#    Sculpt Mode MMB
#===============================================================================
class VIEW3D_MT_malias_mmb_sculpt(VIEW3D_MT_malias_mmb_sculpt_base, Menu):
	bl_label = "Sculpt Transform"
	Brushes = ['Clay Strips', 'Scrape/Peaks', 'SculptDraw', 'Nudge', 'Pinch/Magnify', 'Crease', 'Grab']
	Icon = 'SCULPTMODE_HLT'

	def __init__(self):
		self.CheckBrushes = [x for x in bpy.data.brushes if x.use_paint_sculpt]
		super().__init__()


#===============================================================================
#    Texture Paint MMB
#===============================================================================
class VIEW3D_MT_malias_mmb_image_paint(VIEW3D_MT_malias_mmb_sculpt_base, Menu):
	bl_label = "Texture Paint Transform"
	Brushes = ['TexDraw', 'Soften', 'Smear', 'Mask', 'Clone', 'Fill' ]
	Icon = 'TPAINT_HLT'

	def __init__(self):
		self.CheckBrushes = [x for x in bpy.data.brushes if x.use_paint_image]
		super().__init__()


#===============================================================================
#    Vertex Paint MMB
#===============================================================================
class VIEW3D_MT_malias_mmb_vertex_paint(VIEW3D_MT_malias_mmb_sculpt_base, Menu):
	bl_label = "Vertex Paint Transform"
	Brushes = ['Add', 'Subtract', 'Blur', 'Draw', 'Darken', 'Mix', 'Multiply']
	Icon = 'VPAINT_HLT'

	def __init__(self):
		self.CheckBrushes = [x for x in bpy.data.brushes if x.use_paint_vertex]
		super().__init__()


#===============================================================================
#    Weight Paint MMB
#===============================================================================
class VIEW3D_MT_malias_mmb_weight_paint(VIEW3D_MT_malias_mmb_sculpt_base, Menu):
	bl_label = "Weight Paint Transform"
	Brushes = VIEW3D_MT_malias_mmb_vertex_paint.Brushes
	Icon = 'WPAINT_HLT'

	def __init__(self):
		self.CheckBrushes = [x for x in bpy.data.brushes if x.use_paint_weight]
		super().__init__()


#===============================================================================
#    right mouse button pie menus
#    Starting with base class
#===============================================================================
class VIEW3D_MT_malias_rmb_base(MaliasPie_Base, SetToolExtension):

	def West(self, context):
		self.pie.operator('OBJECT_OT_shade_smooth', text = 'Shade Smooth', icon = 'ANTIALIASED')

	def East(self, context):
		self.pie.operator('OBJECT_OT_shade_flat', text = 'Shade Flat', icon = 'ALIASED')

	def SouthEast(self, context):
		self.pie.prop(context.space_data.overlay, 'show_wireframes', text = 'Show Wire', icon = 'SHADING_WIRE')

	def North(self, context):
		self.pie.operator('object.display_toggle', text = 'Toggle Show In Front', icon = 'XRAY').type = 'show_in_front'

	def NorthWest(self, context):
		self.pie.operator('OBJECT_OT_display_toggle', text = 'Toggle Local Axis', icon = 'AXIS_TOP').type = 'show_axis'

	def NorthEast(self, context):
		self.pie.operator('OBJECT_OT_display_toggle', text = 'Toggle Name Display', icon = 'LONGDISPLAY').type = 'show_name'

	def SouthWest(self, context):
		self.pie.prop(context.space_data.overlay, 'show_face_orientation', text = 'Toggle Face Orientation', icon = 'ORIENTATION_NORMAL')

	def South(self, context):

		col = self.pie.column(align = True)
		row = col.row(align = True)
		row.operator('object.set_draw_type', text = 'Bounds', icon = 'SHADING_BBOX').display_type = 'BOUNDS'
		row.operator('object.set_draw_type', text = 'Wire', icon = 'SHADING_WIRE').display_type = 'WIRE'
		row = col.row(align = True)
		row.operator('object.set_draw_type', text = 'Solid', icon = 'SHADING_SOLID').display_type = 'SOLID'
		row.operator('object.set_draw_type', text = 'Textured', icon = 'SHADING_TEXTURE').display_type = 'TEXTURED'
		row = col.row(align = True)
		row.operator('object.display_toggle', text = 'Bounding Box', icon = 'SHADING_BBOX').type = 'show_bounds'
		row.operator('object.display_toggle', text = 'Texture Space', icon = 'NODE_TEXTURE').type = 'show_texture_space'

		col.separator()

		row = col.row(align = True)
		row.prop(context.scene.render, "use_simplify", icon = 'SCENE_DATA')
		row.prop(context.space_data.overlay, 'show_overlays', icon = 'OVERLAY')

		col.separator()

		row = col.row(align = True)
		_mode = 'film_transparent' if bpy.app.version > (2, 80, 0) else 'alpha_mode'
		row.prop(context.scene.render, _mode , expand = True, icon = 'IMAGE_RGB_ALPHA')
		row = col.row(align = True)
		row.prop(context.space_data, 'lock_camera', icon = 'OUTLINER_OB_CAMERA')
		row.prop(context.space_data, 'lock_cursor', icon = 'PIVOT_CURSOR')


#===============================================================================
#    Object Mode RMB
#===============================================================================
class VIEW3D_MT_malias_rmb_object(VIEW3D_MT_malias_rmb_base, Menu):
	bl_label = "Object Display"


#===============================================================================
#    Sculpt Mode RMB
#===============================================================================
class VIEW3D_MT_malias_rmb_sculpt(VIEW3D_MT_malias_rmb_base, Menu):
	bl_label = "Sculpt Edit"

	def SouthEast(self, context):
		row = self.pie.row(align = True)
		row.operator("brush.curve_preset", icon = 'SMOOTHCURVE', text = "").shape = 'SMOOTH'
		row.operator("brush.curve_preset", icon = 'SPHERECURVE', text = "").shape = 'ROUND'
		row.operator("brush.curve_preset", icon = 'ROOTCURVE', text = "").shape = 'ROOT'
		row.operator("brush.curve_preset", icon = 'SHARPCURVE', text = "").shape = 'SHARP'
		row.operator("brush.curve_preset", icon = 'LINCURVE', text = "").shape = 'LINE'
		row.operator("brush.curve_preset", icon = 'NOCURVE', text = "").shape = 'MAX'

	def North(self, context):
		self.pie.prop(context.scene.tool_settings.sculpt, 'use_symmetry_y', icon = 'AXIS_FRONT')

	def NorthWest(self, context):
		self.pie.prop(context.scene.tool_settings.sculpt, 'use_symmetry_x', icon = 'AXIS_SIDE')

	def NorthEast(self, context):
		self.pie.prop(context.scene.tool_settings.sculpt, 'use_symmetry_z', icon = 'AXIS_TOP')


#===============================================================================
#    Weight Paint RMB
#===============================================================================
class VIEW3D_MT_malias_rmb_weight(VIEW3D_MT_malias_rmb_base, Menu):
	bl_label = "Weight Edit"

	def NorthEast(self, context):
		self.pie.prop(context.scene.tool_settings, 'use_auto_normalize', text = 'Auto-Normalize', icon = 'MOD_VERTEX_WEIGHT')

	def North(self, context):
		self.pie.operator('object.vertex_group_normalize', icon = 'MOD_VERTEX_WEIGHT')

	def NorthWest(self, context):
		self.pie.operator('object.vertex_group_fix', icon = 'UV_VERTEXSEL')

	SouthEast = VIEW3D_MT_malias_rmb_sculpt.SouthEast


#===========================================================================
#    Texture Paint RMB
#===========================================================================
class VIEW3D_MT_malias_rmb_image_paint(VIEW3D_MT_malias_rmb_base, Menu):
	bl_label = "Texture Paint Edit"

	def NorthWest(self, context):
		self.pie.prop(context.scene.tool_settings.image_paint.brush, 'use_accumulate', icon = 'PLUS')

	def NorthEast(self, context):
		self.pie.prop(context.scene.tool_settings.image_paint.brush, 'color', icon = 'COLOR')

	SouthEast = VIEW3D_MT_malias_rmb_sculpt.SouthEast


#===============================================================================
#    Mesh Edit Mode RMB Pie Menu
#    This one replaces the Object one entirely
#===============================================================================
class VIEW3D_MT_malias_rmb_mesh(MaliasPie_Base, SetToolExtension, Menu):
	bl_label = "Mesh Edit"

	def West(self, context:bpy.context):
		self.SetToolByName(name = 'builtin.knife', icon = 'SCULPTMODE_HLT')

	def East(self, context:bpy.context):
		self.pie.operator('mesh.bridge_edge_loops', text = 'Bridge', icon = 'UV_EDGESEL')

	def North(self, context:bpy.context):
		self.SetToolByName(name = 'builtin.loop_cut', icon = 'UV_EDGESEL')

	def South(self, context:bpy.context):
		self.SetToolByName(name = 'builtin.extrude_region', icon = 'TRACKING')

	def NorthEast(self, context:bpy.context):
		self.pie.operator('mesh.dissolve_mode', text = 'Dissolve Edge / Vertex', icon = 'FULLSCREEN_EXIT').use_verts = True

	def NorthWest(self, context):
		self.pie.operator('mesh.merge', text = 'Merge to Center', icon = 'AUTOMERGE_ON').type = 'CENTER'

	def SouthEast (self, context:bpy.context):
		self.SetToolByName(name = 'builtin.extrude_along_normals', icon = 'NORMALS_VERTEX_FACE')

	def SouthWest(self, context):
		try:
			self.pie.operator('mesh.merge', text = 'Merge to Last Selected', icon = 'AUTOMERGE_ON').type = 'LAST'
		except:
			pass


#===============================================================================
#    UV Editor Pie Menus
#===============================================================================
class UV_MT_malias_lmb_uv(SetToolExtension, Menu):
	bl_label = "Select UVs"

	def __init__(self):
		MaliasPie_Base.__init__(self)
		self.isSync = bpy.data.scenes[bpy.context.scene.name].tool_settings.use_uv_select_sync #    @UndefinedVariable

	def draw(self, context):
		#    if we are painting on the image, replace the commands
		if context.space_data.mode == 'PAINT':
			self.North = VIEW3D_MT_malias_lmb_vertex_paint.North
			self.NorthWest = VIEW3D_MT_malias_lmb_vertex_paint.NorthWest
			self.NorthEast = VIEW3D_MT_malias_lmb_vertex_paint.NorthEast

		#    after functions replacement, call the menu
		MaliasPie_Base.draw(self, context)

	def West(self, context):
		if self.isSync:
			self.pie.operator('view3d.select_component', text = 'Select Edges').type = 'EDGE'
		else:
			self.pie.operator('uv.select_component', text = 'Select Edges').type = 'EDGE'

	def East(self, context):
		if self.isSync:
			self.pie.operator('view3d.select_component', text = 'Select Vertices').type = 'VERT'
		else:
			self.pie.operator('uv.select_component', text = 'Select Vertices').type = 'VERTEX'

	def SouthEast(self, context):
		if self.isSync:
			self.pie.operator('view3d.select_component', text = 'Select Vertices').type = 'FACE'
		else:
			self.pie.operator('uv.select_component', text = 'Select Faces').type = 'FACE'

	def South(self, context):
		self.pie.operator('uv.select_component', text = 'Select UV Island').type = 'ISLAND'

	def North(self, context):
		self.pie.operator('uv.select_all', text = 'Select Nothing').action = 'DESELECT'

	def NorthWest(self, context):
		self.SetToolByName(name = 'builtin.select_circle')

	def NorthEast(self, context):
		self.SetToolByName(name = 'builtin.select_box')

	def SouthWest(self, context):
		self.pie.operator('uv.select_component', text = 'Toggle Sync').type = 'TOGGLESYNC'


class UV_MT_malias_mmb_uv(MaliasPie_Base, Menu):
	'''
	possible functions here
	'''
	bl_label = "Transform UVs"

	#    borrow functions and attributes from 3DView Pie Menu
	#    if necessary, values can be changed here later on, but for now we
	#    keep it in sync between 3d and 2d views
	brushAdder = VIEW3D_MT_malias_mmb_sculpt_base.brushAdder
	TextureBrushes = VIEW3D_MT_malias_mmb_image_paint.Brushes

	def draw(self, context):
		#    swap brush menus here
		if context.space_data.mode == 'PAINT':
			for _brush in self.TextureBrushes:
				self.brushAdder(_brush)

		else:
			MaliasPie_Base.draw(self, context)

	def West(self, context):
		self.pie.operator('uv.snap_cursor', text = 'Cursor to Selected').target = 'SELECTED'

	def East(self, context):
		self.pie.operator('uv.align', text = 'Align UVs').axis = 'ALIGN_AUTO'

	def North(self, context):
		self.pie.operator('uv.tool_settings', text = 'Proportional Edit').type = 'PROPORTIONAL'

	def NorthWest(self, context):
		self.pie.operator('uv.snap_selected', text = 'Selection to Cursor').target = 'CURSOR_OFFSET'

	def NorthEast(self, context):
		self.pie.operator('uv.tool_settings', text = 'Sculpt UVs toggle').type = 'UV_SCULPT'

	def SouthEast(self, context):
		self.pie.operator('uv.align', text = 'Straighten UVs').axis = 'ALIGN_S'


class UV_MT_malias_rmb_uv(MaliasPie_Base, Menu):
	'''
	possible functions
	UV display options
	image generation for cycles / texture testing
	layout exporting to external app
	'''

	bl_label = "Layout UV"

	def draw(self, context):
		#    same trick as in 3DView Pie Menus
		if context.space_data.mode == 'PAINT':
			'''nw so no'''
			self.North = lambda context: VIEW3D_MT_malias_rmb_object.North(self, context)
			self.South = lambda context: VIEW3D_MT_malias_rmb_object.South(self, context)
			self.East = lambda context: VIEW3D_MT_malias_rmb_object.East(self, context)
			self.West = lambda context: VIEW3D_MT_malias_rmb_object.West(self, context)
			self.NorthEast = lambda context: VIEW3D_MT_malias_rmb_object.NorthEast(self, context)
			VIEW3D_MT_malias_rmb_object.TexturePaintReplacement(self, context)

		MaliasPie_Base.draw(self, context)

	def NorthWest(self, context):
		self.pie.operator('uv.editor_settings', text = 'Toggle Sticky Select Mode').scope = 'STICKY_SELECT_MODE'

	def South(self, context):
		self.pie.operator('uv.unwrap')

	def NorthEast(self, context):
		self.pie.operator('uv.stitch')

	def North(self, context):
		self.pie.operator('uv.average_islands_scale')

	def East(self, context):
		self.pie.operator('uv.pin', text = 'Pin UVs').clear = False

	def West(self, context):
		self.pie.operator('uv.pin', text = 'Unpin UVs').clear = True

	def SouthEast(self, context):
		self.pie.operator('uv.editor_settings', text = 'Toggle Stretch Display').scope = 'STRETCH'

	def SouthWest(self, context):
		self.pie.operator('uv.editor_settings', text = 'Cycle UV Draw Type').scope = 'EDGE_DRAW_TYPE'

#===============================================================================
#    Node Editor Pie Menus come here
#===============================================================================
#    TODO: Add Node Editor Pie Menus
#    TODO:Add Particle Edit Pie Menus (low prio)


#===============================================================================
#    nested Pie Menus come here
#===============================================================================
class VIEW3D_MT_malias_generative_modifiers(MaliasPie_Base, Menu):
	bl_label = 'Generative Modifiers'

	m = 'object.modifier_add'

	@classmethod
	def poll(cls, context):
		return context.active_object.type == 'MESH'

	def North(self, context):
		self.pie.operator(self.m, text = 'Subdivision Surface', icon = 'MOD_SUBSURF').type = 'SUBSURF'

	def South(self, context):
		self.pie.operator(self.m, text = 'Solidify', icon = 'MOD_SOLIDIFY').type = 'SOLIDIFY'

	def East(self, context):
		self.pie.operator(self.m, text = 'Mirror', icon = 'MOD_MIRROR').type = 'MIRROR'

	def West(self, context):
		self.pie.operator(self.m, text = 'Array', icon = 'MOD_ARRAY').type = 'ARRAY'

	def NorthEast(self, context):
		self.pie.operator(self.m, text = 'Screw', icon = 'MOD_SCREW').type = 'SCREW'

	def NorthWest(self, context):
		self.pie.operator(self.m, text = 'Skin', icon = 'MOD_SKIN').type = 'SKIN'

	def SouthEast(self, context):
		self.pie.operator(self.m, text = 'Bevel', icon = 'MOD_BEVEL').type = 'BEVEL'

	def SouthWest(self, context):
		self.pie.operator_menu_enum("object.modifier_add", "type", icon = 'MODIFIER', text = 'Add Custom...')
